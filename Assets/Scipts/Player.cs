﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player control script
/// </summary>

// Requires the GameObject to have a Rigidbody component
[RequireComponent(typeof(Rigidbody2D))]

public class Player : MonoBehaviour
{
    // for moving and clamping
    private const float MoveUnitsPerSecond = 6f;
    private float verticalInput;
    private float spriteHalfHeight;
    private float clampLowerBound;
    private float clampUpperBound;

    // saved for efficiency
    private float screenLeft;
    private float screenRight;
    private float screenUp;
    private float screenDown;

    // for shooting
    bool canShoot = true;
    Timer shootGapTimer;

    // Start is called before the first frame update
    void Start()
    {
        // get game object height
        spriteHalfHeight = GetComponent<SpriteRenderer>().bounds.size.y * 0.5f;

        // get screen edges in world coordinates
        float screenZ = -Camera.main.transform.position.z;
        Vector3 lowerLeftCornerScreen = new Vector3(0, 0, screenZ);
        Vector3 upperRightCornerScreen = new Vector3(
            Screen.width, Screen.height, screenZ);

        Vector3 lowerLeftCornerWorld =
            Camera.main.ScreenToWorldPoint(lowerLeftCornerScreen);
        Vector3 upperRightCornerWorld =
            Camera.main.ScreenToWorldPoint(upperRightCornerScreen);

        screenLeft = lowerLeftCornerWorld.x;
        screenRight = upperRightCornerWorld.x;
        screenDown = lowerLeftCornerWorld.y;
        screenUp = upperRightCornerWorld.y;

        // get clamp bounds
        clampLowerBound = screenDown + spriteHalfHeight;
        clampUpperBound = screenUp - spriteHalfHeight;

        // add timer to limit number of shoots per second
        shootGapTimer = gameObject.AddComponent<Timer>();
        shootGapTimer.Duration = 1;
        shootGapTimer.AddTimerFinishedListener(allowToShoot);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;

        // moving character on vertical axis
        verticalInput = Input.GetAxis("Vertical");
        if (verticalInput != 0)
        {
            position.y += verticalInput * MoveUnitsPerSecond * Time.deltaTime;
            
            // keep player inside screen lower and upper boundaries
            position.y = Mathf.Clamp(position.y, clampLowerBound, clampUpperBound);
            transform.position = position;
        }

        // shoot the ring
        if (canShoot && Input.GetAxis("Fire1") != 0)
        {
            // forbid to shoot and run timer
            canShoot = false;
            shootGapTimer.Run();

            // shoot the ring
            GameObject ring = Instantiate(Resources.Load("Ring"),
                transform.position, Quaternion.identity) as GameObject;
            ring.GetComponent<Rigidbody2D>().AddForce(Vector3.right * 100, ForceMode2D.Force);
        }
    }

    void allowToShoot()
    {
        canShoot = true;
    }
}
